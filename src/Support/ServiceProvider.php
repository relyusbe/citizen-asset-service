<?php

namespace Support;

use Citizen\Common\Support\Middleware\LogContext;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function boot(Router $router): void
    {
        $router->pushMiddlewareToGroup('api', LogContext::class);
        $router->pushMiddlewareToGroup('web', LogContext::class);
    }
}
